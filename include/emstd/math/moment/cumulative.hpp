///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_MATH_MOMENT_CUMULATIVE_HPP
#define EMSTD_MATH_MOMENT_CUMULATIVE_HPP

#include <limits>
#include <cmath>
#include <emstd/detail/config.hpp>

namespace emstd
{
namespace math
{
namespace moment
{

// Philippe Pebay, "Formulas for Robust, One-Pass Parallel Computation of
//   Covariances and Arbitrary-Order Statistical Moments", technical
//   report SAND2008-6212, 2008.

enum struct with
{
    mean,
    variance,
    skewness,
    kurtosis
};

template <typename, typename, moment::with>
class basic_cumulative;

//-----------------------------------------------------------------------------

template <typename T, typename Size>
class basic_cumulative<T, Size, with::mean>
{
public:
    using value_type = T;
    using size_type = Size;

    // Checks if collection is empty.

    constexpr bool empty() const noexcept
    {
        return size() == 0;
    }

    // Returns number of inserted elements.

    constexpr size_type size() const noexcept
    {
        return member.count;
    }

    // Returns cumulative mean.
    //
    // Mandates:
    // - Not empty()

    constexpr value_type mean() const noexcept
    {
        return member.mean;
    }

    EMSTD_CONSTEXPR_MUTABLE
    void push(value_type number) noexcept
    {
        ++member.count;
        member.mean += (number - member.mean) / value_type(member.count);
    }

    void clear() noexcept
    {
        member.mean = {};
        member.count = {};
    }

protected:
    struct
    {
        value_type mean{};
        size_type count{};
    } member;
};

//-----------------------------------------------------------------------------

template <typename T, typename Size>
class basic_cumulative<T, Size, with::variance>
    : protected basic_cumulative<T, Size, with::mean>
{
protected:
    using base = basic_cumulative<T, Size, with::mean>;

public:
    using typename base::value_type;
    using typename base::size_type;

    using base::empty;
    using base::size;
    using base::mean;

    EMSTD_CONSTEXPR_MUTABLE
    void push(value_type number) noexcept
    {
        const auto diff = number - mean();
        base::push(number);
        member.variance += diff * (number - mean());
    }

    // Returns cumulative variance.
    //
    // Mandates:
    // - Not empty()

    constexpr value_type variance() const noexcept
    {
        return member.variance / value_type(size());
    }

    void clear() noexcept
    {
        base::clear();
        member.variance = {};
    }

protected:
    struct
    {
        value_type variance{};
    } member;
};

//-----------------------------------------------------------------------------

template <typename T, typename Size>
class basic_cumulative<T, Size, with::skewness>
    : protected basic_cumulative<T, Size, with::variance>
{
protected:
    using base = basic_cumulative<T, Size, with::variance>;

public:
    using typename base::value_type;
    using typename base::size_type;

    using base::empty;
    using base::size;
    using base::mean;
    using base::variance;

    EMSTD_CONSTEXPR_MUTABLE
    void push(value_type number) noexcept
    {
        const auto count = size();
        const auto delta = number - mean();
        const auto n = count + 1;
        const auto delta_over_n = delta / n;

        member.skewness += ((delta * delta_over_n * (n - 1) * (n - 2)) - 3 * base::member.variance) * delta_over_n;

        base::push(number);
    }

    // Returns cumulative skewness.
    //
    // Mandates:
    // - Not empty()

    value_type skewness() const noexcept
    {
        if (member.skewness < std::numeric_limits<value_type>::epsilon())
            return {};
        return std::sqrt(size()) * member.skewness / (std::sqrt(base::member.variance) * base::member.variance);
    }

    void clear() noexcept
    {
        base::clear();
        member.skewness = {};
    }

protected:
    struct
    {
        value_type skewness{};
    } member;
};

//-----------------------------------------------------------------------------

template <typename T, typename Size>
class basic_cumulative<T, Size, with::kurtosis>
    : protected basic_cumulative<T, Size, with::skewness>
{
    using base = basic_cumulative<T, Size, with::skewness>;

public:
    using typename base::value_type;
    using typename base::size_type;

    using base::empty;
    using base::size;
    using base::mean;
    using base::variance;
    using base::skewness;

    EMSTD_CONSTEXPR_MUTABLE
    void push(value_type number) noexcept
    {
        const auto count = size();
        const auto delta = number - mean();
        const auto n = count + 1;
        const auto delta_over_n = delta / n;

        const auto expr = delta * delta_over_n * (n - 1) * (n * (n - 3) + 3);
        const auto var_expr = value_type(6) * base::base::member.variance;
        const auto skewness_expr = value_type(4) * base::member.skewness;
        member.kurtosis += ((expr + var_expr) * delta_over_n - skewness_expr) * delta_over_n;

        base::push(number);
    }

    // Returns cumulative kurtosis.
    //
    // Mandates:
    // - Not empty()

    value_type kurtosis() const noexcept
    {
        if (member.kurtosis < std::numeric_limits<value_type>::epsilon())
            return {};
        return size() * member.kurtosis / (base::base::member.variance * base::base::member.variance);
    }

    void clear() noexcept
    {
        base::clear();
        member.kurtosis = {};
    }

protected:
    struct
    {
        value_type kurtosis{};
    } member;
};

//-----------------------------------------------------------------------------

template <typename T>
using cumulative_mean = basic_cumulative<T, unsigned, with::mean>;

template <typename T>
using cumulative_variance = basic_cumulative<T, unsigned, with::variance>;

template <typename T>
using cumulative_skewness = basic_cumulative<T, unsigned, with::skewness>;

template <typename T>
using cumulative_kurtosis = basic_cumulative<T, unsigned, with::kurtosis>;

template <typename T>
using cumulative = cumulative_kurtosis<T>;

} // namespace moment
} // namespace math
} // namespace emstd

#endif // EMSTD_MATH_MOMENT_CUMULATIVE_HPP
