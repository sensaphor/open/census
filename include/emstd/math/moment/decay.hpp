///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_MATH_MOMENT_DECAY_HPP
#define EMSTD_MATH_MOMENT_DECAY_HPP

#include <emstd/type_traits.hpp>
#include <emstd/ext/numeric_traits.hpp>
#include <emstd/ratio.hpp>

namespace emstd
{
namespace math
{
namespace moment
{
namespace detail
{

template <typename T>
T square(T number) noexcept
{
    return number * number;
}

} // namespace detail

//-----------------------------------------------------------------------------

template <typename, typename...>
class decay;

template <typename T, typename Mean>
class decay<T, Mean>
{
    static_assert(std::ratio_greater<Mean, std::ratio<0,1>>::value, "Mean factor must be greater than zero");
    static_assert(std::ratio_less_equal<Mean, std::ratio<1,1>>::value, "Mean factor must be less than one");

public:
    using value_type = T;

    constexpr decay() noexcept = default;

    decay(value_type init)
    {
        member.mean = init;
    }

    constexpr value_type mean() const noexcept
    {
        return member.mean;
    }

    void push(value_type number) noexcept
    {
        member.mean += mean_factor * (number - member.mean);
    }

protected:
    using scalar_type = numeric_scalar_t<value_type>;
    static constexpr scalar_type mean_factor = (Mean::num /  scalar_type(Mean::den));

    struct
    {
        value_type mean{};
    } member;
};

template <typename T, typename Mean, typename Variance>
class decay<T, Mean, Variance>
    : protected decay<T, Mean>
{
    using base = decay<T, Mean>;

    static_assert(std::ratio_greater<Variance, std::ratio<0,1>>::value, "Variance factor must be greater than zero");
    static_assert(std::ratio_less_equal<Variance, std::ratio<1,1>>::value, "Variance factor must be less than one");

public:
    using typename base::value_type;

    using base::base;
    using base::mean;

    constexpr value_type variance() const noexcept
    {
        return member.variance;
    }

    void push(value_type number) noexcept
    {
        base::push(number);
        const value_type delta = number - mean();
        member.variance += variance_factor * (detail::square(delta) - member.variance);
    }

protected:
    using typename base::scalar_type;
    static constexpr scalar_type variance_factor = (Variance::num /  scalar_type(Variance::den));

    struct
    {
        value_type variance{};
    } member;
};

template <typename T, typename Mean, typename Variance, typename Skewness>
class decay<T, Mean, Variance, Skewness>
    : protected decay<T, Mean, Variance>
{
    using base = decay<T, Mean, Variance>;

    static_assert(std::ratio_greater<Skewness, std::ratio<0,1>>::value, "Skewness factor must be greater than zero");
    static_assert(std::ratio_less_equal<Skewness, std::ratio<1,1>>::value, "Skewness factor must be less than one");

public:
    using typename base::value_type;

    using base::base;
    using base::mean;
    using base::variance;

    constexpr value_type skewness() const noexcept
    {
        return member.skewness;
    }

    void push(value_type number) noexcept
    {
        base::push(number);
        const value_type delta = number - mean();
        member.skewness += skewness_factor * (detail::square(delta) * delta - member.skewness);
    }

protected:
    using typename base::scalar_type;
    static constexpr scalar_type skewness_factor = (Skewness::num /  scalar_type(Skewness::den));

    struct
    {
        value_type skewness{};
    } member;
};

template <typename T, typename Mean, typename Variance, typename Skewness, typename Kurtosis>
class decay<T, Mean, Variance, Skewness, Kurtosis>
    : protected decay<T, Mean, Variance, Skewness>
{
    using base = decay<T, Mean, Variance, Skewness>;

    static_assert(std::ratio_greater<Kurtosis, std::ratio<0,1>>::value, "Kurtosis factor must be greater than zero");
    static_assert(std::ratio_less_equal<Kurtosis, std::ratio<1,1>>::value, "Kurtosis factor must be less than one");

public:
    using typename base::value_type;

    using base::base;
    using base::mean;
    using base::variance;
    using base::skewness;

    constexpr value_type kurtosis() const noexcept
    {
        return (variance() == value_type(0)) ? value_type(0) : member.kurtosis / detail::square(variance());
    }

    void push(value_type number) noexcept
    {
        base::push(number);
        const value_type delta = number - mean();
        member.kurtosis += kurtosis_factor * (detail::square(delta) * detail::square(delta) - member.kurtosis);
    }

protected:
    using typename base::scalar_type;
    static constexpr scalar_type kurtosis_factor = (Kurtosis::num /  scalar_type(Kurtosis::den));

    struct
    {
        value_type kurtosis{};
    } member;
};

} // namespace moment
} // namespace math
} // namespace emstd

#endif // EMSTD_MATH_MOMENT_DECAY_HPP
