///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/functional.hpp>
#include <emstd/math/moment/cumulative.hpp>

using namespace emstd::math;

//-----------------------------------------------------------------------------

namespace suite_mean
{

void api_empty()
{
    moment::cumulative_mean<double> filter;
    assert(filter.empty());
    filter.push(1.0);
    assert(!filter.empty());
}

void api_size()
{
    moment::cumulative_mean<double> filter;
    assert(filter.size() == 0);
    filter.push(1.0);
    assert(filter.size() == 1);
}

void api_mean()
{
    moment::cumulative_mean<double> filter;
    filter.push(1.0);
    assert(filter.mean() == 1.0);
}

void api_clear()
{
    moment::cumulative_mean<double> filter;
    filter.push(1.0);
    assert(filter.size() == 1);
    filter.clear();
    assert(filter.size() == 0);
}

void run()
{
    api_empty();
    api_size();
    api_mean();
    api_clear();
}

} // namespace suite_mean

//-----------------------------------------------------------------------------

namespace suite_variance
{

void api_variance()
{
    moment::cumulative_variance<double> filter;
    filter.push(1.0);
    assert(filter.variance() == 0.0);
}

void api_clear()
{
    moment::cumulative_variance<double> filter;
    filter.push(1.0);
    assert(filter.size() == 1);
    filter.clear();
    assert(filter.size() == 0);
}

void run()
{
    api_variance();
    api_clear();
}

} // namespace suite_variance

//-----------------------------------------------------------------------------

namespace suite_skewness
{

void api_skewness()
{
    moment::cumulative_skewness<double> filter;
    filter.push(1.0);
    assert(filter.skewness() == 0.0);
}

void api_clear()
{
    moment::cumulative_skewness<double> filter;
    filter.push(1.0);
    assert(filter.size() == 1);
    filter.clear();
    assert(filter.size() == 0);
}

void run()
{
    api_skewness();
    api_clear();
}

} // namespace suite_variance

//-----------------------------------------------------------------------------

namespace suite_kurtosis
{

void api_kurtosis()
{
    moment::cumulative_kurtosis<double> filter;
    filter.push(1.0);
    assert(filter.kurtosis() == 0.0);
}

void api_clear()
{
    moment::cumulative_kurtosis<double> filter;
    filter.push(1.0);
    assert(filter.size() == 1);
    filter.clear();
    assert(filter.size() == 0);
}

void run()
{
    api_kurtosis();
    api_clear();
}

} // namespace suite_kurtosis

//-----------------------------------------------------------------------------

namespace suite_sequence
{

constexpr const auto equals = emstd::close_to<double>(1e-6);

void sequence_same()
{
    moment::cumulative<double> filter;
    filter.push(1.0);
    assert(filter.size() == 1);
    assert(equals(filter.mean(), 1.0));
    assert(equals(filter.variance(), 0.0));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 0.0));
    filter.push(1.0);
    assert(filter.size() == 2);
    assert(equals(filter.mean(), 1.0));
    assert(equals(filter.variance(), 0.0));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 0.0));
    filter.push(1.0);
    assert(filter.size() == 3);
    assert(equals(filter.mean(), 1.0));
    assert(equals(filter.variance(), 0.0));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 0.0));
}

void sequence_linear_increase()
{
    moment::cumulative<double> filter;
    filter.push(1.0);
    assert(equals(filter.mean(), 1.0));
    assert(equals(filter.variance(), 0.0));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 0.0));
    filter.push(2.0);
    assert(equals(filter.mean(), 1.5));
    assert(equals(filter.variance(), 0.25));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.0));
    filter.push(3.0);
    assert(equals(filter.mean(), 2.0));
    assert(equals(filter.variance(), 0.666667));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.5));
    filter.push(4.0);
    assert(equals(filter.mean(), 2.5));
    assert(equals(filter.variance(), 1.25));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.64));
    filter.push(5.0);
    assert(equals(filter.mean(), 3.0));
    assert(equals(filter.variance(), 2.0));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.7));
    filter.push(6.0);
    assert(equals(filter.mean(), 3.5));
    assert(equals(filter.variance(), 2.916667));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.731429));
    filter.push(7.0);
    assert(equals(filter.mean(), 4.0));
    assert(equals(filter.variance(), 4.0));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.75));
    filter.push(8.0);
    assert(equals(filter.mean(), 4.5));
    assert(equals(filter.variance(), 5.25));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.761905));
    filter.push(9.0);
    assert(equals(filter.mean(), 5.0));
    assert(equals(filter.variance(), 6.666667));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.77));
    filter.push(10.0);
    assert(equals(filter.mean(), 5.5));
    assert(equals(filter.variance(), 8.25));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.775758));
}

void sequence_linear_decrease()
{
    moment::cumulative<double> filter;
    filter.push(10.0);
    assert(equals(filter.mean(), 10.0));
    assert(equals(filter.variance(), 0.0));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 0.0));
    filter.push(9.0);
    assert(equals(filter.mean(), 9.5));
    assert(equals(filter.variance(), 0.25));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.0));
    filter.push(8.0);
    assert(equals(filter.mean(), 9.0));
    assert(equals(filter.variance(), 0.6666667));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.5));
    filter.push(7.0);
    assert(equals(filter.mean(), 8.5));
    assert(equals(filter.variance(), 1.25));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.64));
    filter.push(6.0);
    assert(equals(filter.mean(), 8.0));
    assert(equals(filter.variance(), 2.0));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.7));
    filter.push(5.0);
    assert(equals(filter.mean(), 7.5));
    assert(equals(filter.variance(), 2.916667));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.731429));
    filter.push(4.0);
    assert(equals(filter.mean(), 7.0));
    assert(equals(filter.variance(), 4.0));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.75));
    filter.push(3.0);
    assert(equals(filter.mean(), 6.5));
    assert(equals(filter.variance(), 5.25));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.761905));
    filter.push(2.0);
    assert(equals(filter.mean(), 6.0));
    assert(equals(filter.variance(), 6.666667));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.77));
    filter.push(1.0);
    assert(equals(filter.mean(), 5.5));
    assert(equals(filter.variance(), 8.25));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.775758));
}

void sequence_linear_scatter()
{
    moment::cumulative<double> filter;
    filter.push(1.0);
    assert(equals(filter.mean(), 1.0));
    assert(equals(filter.variance(), 0.0));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 0.0));
    filter.push(3.0);
    assert(equals(filter.mean(), 2.0));
    assert(equals(filter.variance(), 1.0));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.0));
    filter.push(10.0);
    assert(equals(filter.mean(), 4.6666667));
    assert(equals(filter.variance(), 14.888889));
    assert(equals(filter.skewness(), 0.567317));
    assert(equals(filter.kurtosis(), 1.5));
    filter.push(5.0);
    assert(equals(filter.mean(), 4.75));
    assert(equals(filter.variance(), 11.1875));
    assert(equals(filter.skewness(), 0.57874));
    assert(equals(filter.kurtosis(), 1.931182));
    filter.push(8.0);
    assert(equals(filter.mean(), 5.4));
    assert(equals(filter.variance(), 10.64));
    assert(equals(filter.skewness(), 0.091279));
    assert(equals(filter.kurtosis(), 1.592543));
    filter.push(4.0);
    assert(equals(filter.mean(), 5.1666667));
    assert(equals(filter.variance(), 9.1388889));
    assert(equals(filter.skewness(), 0.311017));
    assert(equals(filter.kurtosis(), 1.866806));
    filter.push(7.0);
    assert(equals(filter.mean(), 5.4285714));
    assert(equals(filter.variance(), 8.244898));
    assert(equals(filter.skewness(), 0.073889));
    assert(equals(filter.kurtosis(), 1.912729));
    filter.push(2.0);
    assert(equals(filter.mean(), 5.0));
    assert(equals(filter.variance(), 8.5));
    assert(equals(filter.skewness(), 0.302645));
    assert(equals(filter.kurtosis(), 1.861592));
    filter.push(9.0);
    assert(equals(filter.mean(), 5.4444444));
    assert(equals(filter.variance(), 9.135802));
    assert(equals(filter.skewness(), 0.054644));
    assert(equals(filter.kurtosis(), 1.610906));
    filter.push(6.0);
    assert(equals(filter.mean(), 5.5));
    assert(equals(filter.variance(), 8.25));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.775758));
}

void sequence_exponential_increase()
{
    moment::cumulative<double> filter;
    filter.push(1e0);
    assert(equals(filter.mean(), 1.0));
    assert(equals(filter.variance(), 0.0));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 0.0));
    filter.push(1e1);
    assert(equals(filter.mean(), 5.5));
    assert(equals(filter.variance(), 20.25));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.0));
    filter.push(1e2);
    assert(equals(filter.mean(), 37.0));
    assert(equals(filter.variance(), 1998.0));
    assert(equals(filter.skewness(), 0.685667));
    assert(equals(filter.kurtosis(), 1.5));
    filter.push(1e3);
    assert(equals(filter.mean(), 277.75));
    assert(equals(filter.variance(), 175380.2));
    assert(equals(filter.skewness(), 1.125822));
    assert(equals(filter.kurtosis(), 2.309281));
    filter.push(1e4);
    assert(equals(filter.mean(), 2222.2));
    assert(equals(filter.variance(), 1.526385e7));
    assert(equals(filter.skewness(), 1.46675));
    assert(equals(filter.kurtosis(), 3.202224));
    filter.push(1e5);
    assert(equals(filter.mean(), 18518.5));
    assert(equals(filter.variance(), 1.3405667e9));
    assert(equals(filter.skewness(), 1.75227));
    assert(equals(filter.kurtosis(), 4.129655));
    filter.push(1e6);
    assert(equals(filter.mean(), 158730.0));
    assert(equals(filter.variance(), 1.191049e11));
    assert(equals(filter.skewness(), 2.00181));
    assert(equals(filter.kurtosis(), 5.074546));
    filter.push(1e7);
    assert(equals(filter.mean(), 1.388888e6));
    assert(equals(filter.variance(), 1.069725e13));
    assert(equals(filter.skewness(), 2.22579));
    assert(equals(filter.kurtosis(), 6.029472));
    filter.push(1e8);
    assert(equals(filter.mean(), 1.234567e7));
    assert(equals(filter.variance(), 9.699187e14));
    assert(equals(filter.skewness(), 2.430514));
    assert(equals(filter.kurtosis(), 6.990692));
    filter.push(1e9);
    assert(equals(filter.mean(), 1.111111e8));
    assert(equals(filter.variance(), 8.866442e16));
    assert(equals(filter.skewness(), 2.62009));
    assert(equals(filter.kurtosis(), 7.956116));
}

void sequence_exponential_decrease()
{
    moment::cumulative<double> filter;
    filter.push(1e9);
    assert(equals(filter.mean(), 1e9));
    assert(equals(filter.variance(), 0.0));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 0.0));
    filter.push(1e8);
    assert(equals(filter.mean(), 5.5e8));
    assert(equals(filter.variance(), 2.025e17));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.0));
    filter.push(1e7);
    assert(equals(filter.mean(), 3.7e8));
    assert(equals(filter.variance(), 1.998e17));
    assert(equals(filter.skewness(), 0.685668));
    assert(equals(filter.kurtosis(), 1.5));
    filter.push(1e6);
    assert(equals(filter.mean(), 2.7775e8));
    assert(equals(filter.variance(), 1.753802e17));
    assert(equals(filter.skewness(), 1.1258226));
    assert(equals(filter.kurtosis(), 2.309281));
    filter.push(1e5);
    assert(equals(filter.mean(), 2.2222e8));
    assert(equals(filter.variance(), 1.526385e17));
    assert(equals(filter.skewness(), 1.466751));
    assert(equals(filter.kurtosis(), 3.202224));
    filter.push(1e4);
    assert(equals(filter.mean(), 1.85185e8));
    assert(equals(filter.variance(), 1.340567e17));
    assert(equals(filter.skewness(), 1.752270));
    assert(equals(filter.kurtosis(), 4.129655));
    filter.push(1e3);
    assert(equals(filter.mean(), 1.587301e8));
    assert(equals(filter.variance(), 1.191049e17));
    assert(equals(filter.skewness(), 2.001809));
    assert(equals(filter.kurtosis(), 5.074546));
    filter.push(1e2);
    assert(equals(filter.mean(), 1.388889e8));
    assert(equals(filter.variance(), 1.069725e17));
    assert(equals(filter.skewness(), 2.225792));
    assert(equals(filter.kurtosis(), 6.029472));
    filter.push(1e1);
    assert(equals(filter.mean(), 1.234568e8));
    assert(equals(filter.variance(), 9.699187e16));
    assert(equals(filter.skewness(), 2.430514));
    assert(equals(filter.kurtosis(), 6.990692));
    filter.push(1e0);
    assert(equals(filter.mean(), 1.111111e8));
    assert(equals(filter.variance(), 8.866442e16));
    assert(equals(filter.skewness(), 2.62009));
    assert(equals(filter.kurtosis(), 7.956116));
}

void sequence_exponential_scatter()
{
    moment::cumulative<double> filter;
    filter.push(1e0);
    assert(equals(filter.mean(), 1.0));
    assert(equals(filter.variance(), 0.0));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 0.0));
    filter.push(1e2);
    assert(equals(filter.mean(), 50.5));
    assert(equals(filter.variance(), 2450.25));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 1.0));
    filter.push(1e9);
    assert(equals(filter.mean(), 3.333333e8));
    assert(equals(filter.variance(), 2.222222e17));
    assert(equals(filter.skewness(), 0.707107));
    assert(equals(filter.kurtosis(), 1.5));
    filter.push(1e4);
    assert(equals(filter.mean(), 2.500025e8));
    assert(equals(filter.variance(), 1.874987e17));
    assert(equals(filter.skewness(), 1.154701));
    assert(equals(filter.kurtosis(), 2.333333));
    filter.push(1e7);
    assert(equals(filter.mean(), 2.02002e8));
    assert(equals(filter.variance(), 1.592152e17));
    assert(equals(filter.skewness(), 1.499648));
    assert(equals(filter.kurtosis(), 3.249527));
    filter.push(1e3);
    assert(equals(filter.mean(), 1.683352e8));
    assert(equals(filter.variance(), 1.383466e17));
    assert(equals(filter.skewness(), 1.788468));
    assert(equals(filter.kurtosis(), 4.199304));
    filter.push(1e6);
    assert(equals(filter.mean(), 1.444302e8));
    assert(equals(filter.variance(), 1.220115e17));
    assert(equals(filter.skewness(), 2.040838));
    assert(equals(filter.kurtosis(), 5.165781));
    filter.push(1e1);
    assert(equals(filter.mean(), 1.263764e8));
    assert(equals(filter.variance(), 1.090416e17));
    assert(equals(filter.skewness(), 2.267354));
    assert(equals(filter.kurtosis(), 6.141758));
    filter.push(1e8);
    assert(equals(filter.mean(), 1.234457e8));
    assert(equals(filter.variance(), 9.699461e16));
    assert(equals(filter.skewness(), 2.430501));
    assert(equals(filter.kurtosis(), 6.990652));
    filter.push(1e5);
    assert(equals(filter.mean(), 1.111111e8));
    assert(equals(filter.variance(), 8.866442e16));
    assert(equals(filter.skewness(), 2.62009));
    assert(equals(filter.kurtosis(), 7.956116));
}

void run()
{
    sequence_same();
    sequence_linear_increase();
    sequence_linear_decrease();
    sequence_linear_scatter();
    sequence_exponential_increase();
    sequence_exponential_decrease();
    sequence_exponential_scatter();
}

} // namespace suite_sequence

//-----------------------------------------------------------------------------

int main()
{
    suite_mean::run();
    suite_variance::run();
    suite_skewness::run();
    suite_kurtosis::run();
    suite_sequence::run();
    return 0;
}
