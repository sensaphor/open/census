///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/functional.hpp>
#include <emstd/math/moment/decay.hpp>

using namespace emstd::math;

using semi_ratio = std::ratio<1,2>;
using quadrant_ratio = std::ratio<1,4>;
using octant_ratio = std::ratio<1,8>;

//-----------------------------------------------------------------------------

namespace suite_sequence
{

constexpr const auto equals = emstd::close_to<double>(1e-6);

void sequence_same()
{
    moment::decay<double, octant_ratio, octant_ratio, octant_ratio, octant_ratio> filter{1.0};
    filter.push(1.0);
    assert(equals(filter.mean(), 1.0));
    assert(equals(filter.variance(), 0.0));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 0.0));
    filter.push(1.0);
    assert(equals(filter.mean(), 1.0));
    assert(equals(filter.variance(), 0.0));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 0.0));
    filter.push(1.0);
    assert(equals(filter.mean(), 1.0));
    assert(equals(filter.variance(), 0.0));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 0.0));
}

void sequence_linear_increase()
{
    moment::decay<double, octant_ratio, octant_ratio, octant_ratio, octant_ratio> filter{1.0};
    filter.push(1.0);
    assert(equals(filter.mean(), 1.0));
    assert(equals(filter.variance(), 0.0));
    assert(equals(filter.skewness(), 0.0));
    assert(equals(filter.kurtosis(), 0.0));
    filter.push(2.0);
    assert(equals(filter.mean(), 1.125));
    assert(equals(filter.variance(), 0.095703));
    assert(equals(filter.skewness(), 0.083740));
    assert(equals(filter.kurtosis(), 8.0));
    filter.push(3.0);
    assert(equals(filter.mean(), 1.359375));
    assert(equals(filter.variance(), 0.420197));
    assert(equals(filter.skewness(), 0.625271));
    assert(equals(filter.kurtosis(), 5.492230));
    filter.push(4.0);
    assert(equals(filter.mean(), 1.689453));
    assert(equals(filter.variance(), 1.035));
    assert(equals(filter.skewness(), 2.089006));
    assert(equals(filter.kurtosis(), 4.117840));
    filter.push(5.0);
    assert(equals(filter.mean(), 2.103271));
    assert(equals(filter.variance(), 1.954505));
    assert(equals(filter.skewness(), 4.866199));
    assert(equals(filter.kurtosis(), 3.314302));
    filter.push(6.0);
    assert(equals(filter.mean(), 2.590362));
    assert(equals(filter.variance(), 3.163395));
    assert(equals(filter.skewness(), 9.21282));
    assert(equals(filter.kurtosis(), 2.795296));
    filter.push(7.0);
    assert(equals(filter.mean(), 3.141567));
    assert(equals(filter.variance(), 4.628909));
    assert(equals(filter.skewness(), 15.241523));
    assert(equals(filter.kurtosis(), 2.435308));
    filter.push(8.0);
    assert(equals(filter.mean(), 3.748871));
    assert(equals(filter.variance(), 6.309307));
    assert(equals(filter.skewness(), 22.939682));
    assert(equals(filter.kurtosis(), 2.172546));
    filter.push(9.0);
    assert(equals(filter.mean(), 4.405262));
    assert(equals(filter.variance(), 8.159595));
    assert(equals(filter.skewness(), 32.197513));
    assert(equals(filter.kurtosis(), 1.973377));
    filter.push(10.0);
    assert(equals(filter.mean(), 5.104605));
    assert(equals(filter.variance(), 10.135258));
    assert(equals(filter.skewness(), 42.837529));
    assert(equals(filter.kurtosis(), 1.818005));
}

void run()
{
    sequence_same();
    sequence_linear_increase();
}

} // namespace suite_sequence

//-----------------------------------------------------------------------------

int main()
{
    suite_sequence::run();
    return 0;
}
